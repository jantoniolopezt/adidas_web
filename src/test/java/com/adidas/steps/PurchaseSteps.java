package com.adidas.steps;

import com.adidas.models.PurchaseModel;
import com.adidas.pages.CartPage;
import com.adidas.pages.ConfirmationPage;
import com.adidas.pages.HomePage;
import com.adidas.pages.ProductPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.math.BigDecimal;

public class PurchaseSteps {

    public HomePage homePage;
    public ProductPage productPage;
    public CartPage cartPage;
    public ConfirmationPage confirmationPage;

    @Step("Given the user is on demoblaze store")
    public void openStore() {
        homePage.open();
    }

    @Step("When the user navigate to {0} category")
    public void navigateToCategory(String category) {
        homePage.clickOnCategory(category);
    }

    @Step("Then the products grid should be visible")
    public void productGridIsVisible() {
        Assert.assertTrue(homePage.isProductsGridVisible());
    }

    @Step("When I add the product {0} to the cart and go back to home")
    public void addProductToCartBackHome(String product) {
        homePage.clickOnProduct(product);
        productPage.clickOnAddToCart();
        productPage.acceptConfirmationAlert();
        homePage.backHome();
    }

    @Step("When I navigate to cart")
    public void navigateToCart() {
        homePage.clickOnCart();
    }

    @Step("Then I land in the cart page")
    public void validateCartPageVisible() {
        Assert.assertTrue("Products table is not visible", cartPage.isProductsTableVisible());
    }

    @Step("When I delete product {0} from the cart")
    public void deleteProduct(String product) {
        cartPage.deleteProduct(product);
    }

    @Step("When I store the price of {0} from the cart")
    public BigDecimal storeProductPriceFromCart(String product) {
        return cartPage.getProductPrice(product);
    }

    @Step("When I store the total amount from the cart")
    public BigDecimal storeTotalAmountFromCart() {
        return cartPage.getTotalAmount();
    }

    @Step("Then the price of ${0} should be the same as the total amount of the cart ${1}")
    public void validateTotalAmountFromCart(BigDecimal productPrice, BigDecimal totalAmount) {
        Assert.assertEquals("Total amount from cart is not correct", 0, productPrice.compareTo(totalAmount));
    }

    @Step("When I place the order")
    public void navigateToPlaceOrder() {
        cartPage.clickOnPlaceOrder();
    }

    @Step("Then the order form is visible")
    public void validateOrderFormVisible() {
        Assert.assertTrue("Order form is not visible", cartPage.isOrderModalVisible());
    }

    @Step("When I fill the order form and purchase")
    public void fillOrderFormAndPurchase() {
        cartPage.fillOrderModal();
        cartPage.clickOnPurchase();
    }

    @Step("Then the confirmation is visible")
    public void validateConfirmationPageVisible() {
        Assert.assertTrue("Confirmation is not visible", confirmationPage.isConfirmationInfoVisible());
    }

    @Step("When I store the confirmation values")
    public PurchaseModel storeConfirmationInfo() {
        return confirmationPage.getPurchaseInfo();
    }

    @Step("Then the purchase amount ${0} should be equal to confirmation amount ${1}")
    public void validatePurchaseAmount(BigDecimal purchase, BigDecimal confirmation) {
        Assert.assertEquals("Purchase total amount is not correct", 0, purchase.compareTo(confirmation));
    }

    @Step("Then I finish the purchase process")
    public void finishPurchaseProcess() {
        confirmationPage.clickOnOkBtn();
    }

}
