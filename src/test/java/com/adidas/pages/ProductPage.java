package com.adidas.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProductPage extends PageObject {

    public void clickOnAddToCart() {
        $("//a[contains(text(), 'Add to cart')]")
                .waitUntilVisible().click();
    }

    public void acceptConfirmationAlert() {
        waitFor(ExpectedConditions.alertIsPresent());
        getAlert().accept();
    }

}
