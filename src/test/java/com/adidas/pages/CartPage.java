package com.adidas.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;

import java.math.BigDecimal;

public class CartPage extends PageObject {

    @FindBy(xpath = "//h2[contains(text(), 'Products')]/following-sibling::div/table")
    WebElementFacade productsTable;

    @FindBy(id = "totalp")
    WebElementFacade totalAmount;

    @FindBy(xpath = "//button[contains(text(), 'Place Order')]")
    WebElementFacade placeOrderBtn;

    @FindBy(xpath = "//button[contains(text(), 'Purchase')]")
    WebElementFacade purchaseBtn;

    @FindBy(id = "orderModal")
    WebElementFacade orderModal;

    @FindBy(id = "name")
    WebElementFacade nameField;

    @FindBy(id = "country")
    WebElementFacade countryField;

    @FindBy(id = "city")
    WebElementFacade cityField;

    @FindBy(id = "card")
    WebElementFacade cardField;

    @FindBy(id = "month")
    WebElementFacade monthField;

    @FindBy(id = "year")
    WebElementFacade yearField;

    public boolean isProductsTableVisible() {
        return element(productsTable).waitUntilVisible().isDisplayed();
    }

    public void deleteProduct(String product) {
        $(String.format("(//td[contains(text(), '%s')]/following-sibling::td)[2]/a", product)).click();
    }

    public BigDecimal getTotalAmount() {
        return new BigDecimal(totalAmount.waitUntilVisible().getText());
    }

    public BigDecimal getProductPrice(String product) {
        element(productsTable).waitUntilVisible();
        return new BigDecimal($(String.format("(//td[contains(text(), '%s')]/following-sibling::td)[1]", product))
                .waitUntilVisible().getText());
    }

    public void clickOnPlaceOrder() {
        element(placeOrderBtn).waitUntilVisible().click();
    }

    public boolean isOrderModalVisible() {
        return element(orderModal).waitUntilVisible().isDisplayed();
    }

    public void fillOrderModal() {
        element(nameField).type("Adidas");
        element(countryField).type("Spain");
        element(cityField).type("Barcelona");
        element(cardField).type("5555555555554444");
        element(monthField).type("01");
        element(yearField).type("22");
    }

    public void clickOnPurchase() {
        element(purchaseBtn).waitUntilVisible().click();
    }

}
