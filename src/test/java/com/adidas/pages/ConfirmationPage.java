package com.adidas.pages;

import com.adidas.models.PurchaseModel;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class ConfirmationPage extends PageObject {

    @FindBy(xpath = "//div[contains(@class, 'sa-success')]/following-sibling::p")
    WebElementFacade purchaseInfo;

    @FindBy(xpath = "//button[@class='confirm btn btn-lg btn-primary']")
    WebElementFacade okBtn;

    public boolean isConfirmationInfoVisible() {
        return element(purchaseInfo).waitUntilVisible().isDisplayed();
    }

    public PurchaseModel getPurchaseInfo() {
        PurchaseModel model = new PurchaseModel();
        String rawInfo = purchaseInfo.waitUntilVisible().getText();
        List<String> infoRows = Arrays.asList(rawInfo.split("\\n"));
        model.setConfirmationId(infoRows.get(0).replaceAll("\\D+", "").trim());
        model.setTotalAmountConfirmation(new BigDecimal(infoRows.get(1).replaceAll("\\D+", "").trim()));
        return model;
    }

    public void clickOnOkBtn() {
        element(okBtn).waitUntilVisible().click();
    }

}
