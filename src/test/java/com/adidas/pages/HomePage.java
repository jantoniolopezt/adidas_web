package com.adidas.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.demoblaze.com/index.html")
public class HomePage extends PageObject {

    @FindBy(id = "narvbarx")
    WebElementFacade navigationBar;

    @FindBy(id = "tbodyid")
    WebElementFacade productsGrid;

    @FindBy(id = "nava")
    WebElementFacade logo;

    @FindBy(id = "cartur")
    WebElementFacade cartBtn;

    @WhenPageOpens
    public void waitForNavBar() {
        element(navigationBar).waitUntilVisible();
    }

    public void clickOnCategory(String category) {
        $(String.format("//a[contains(text(), '%s')]", category))
                .waitUntilVisible().click();
    }

    public boolean isProductsGridVisible() {
        return productsGrid.isDisplayed();
    }

    public void clickOnProduct(String product) {
        $(String.format("//div[@id='tbodyid']//a[contains(text(), '%s')]", product))
                .waitUntilVisible().click();
    }

    public void backHome() {
        logo.waitUntilVisible().click();
    }

    public void clickOnCart() {
        element(cartBtn).waitUntilVisible().click();
    }

}
