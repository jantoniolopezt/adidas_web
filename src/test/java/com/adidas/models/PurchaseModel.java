package com.adidas.models;

import java.math.BigDecimal;

public class PurchaseModel {

    private BigDecimal productPrice;
    private BigDecimal totalAmountCart;
    private String confirmationId;
    private BigDecimal totalAmountConfirmation;

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public BigDecimal getTotalAmountCart() {
        return totalAmountCart;
    }

    public void setTotalAmountCart(BigDecimal totalAmountCart) {
        this.totalAmountCart = totalAmountCart;
    }

    public String getConfirmationId() {
        return confirmationId;
    }

    public void setConfirmationId(String confirmationId) {
        this.confirmationId = confirmationId;
    }

    public BigDecimal getTotalAmountConfirmation() {
        return totalAmountConfirmation;
    }

    public void setTotalAmountConfirmation(BigDecimal totalAmountConfirmation) {
        this.totalAmountConfirmation = totalAmountConfirmation;
    }
}
