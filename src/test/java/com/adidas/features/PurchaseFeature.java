package com.adidas.features;

import com.adidas.models.PurchaseModel;
import com.adidas.steps.PurchaseSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class PurchaseFeature {

    @Managed
    public WebDriver driver;

    @Steps
    public PurchaseSteps steps;

    public PurchaseModel purchaseModel = new PurchaseModel();
    public PurchaseModel confirmation;

    @Test
    @Title("Validate general navigation, add and remove articles and place an order")
    public void placeAnOrder() {
        steps.openStore();
        steps.navigateToCategory("Phones");
        steps.productGridIsVisible();
        steps.navigateToCategory("Laptops");
        steps.productGridIsVisible();
        steps.navigateToCategory("Monitors");
        steps.productGridIsVisible();
        steps.navigateToCategory("Laptops");
        steps.addProductToCartBackHome("Sony vaio i5");
        steps.navigateToCategory("Laptops");
        steps.addProductToCartBackHome("Dell i7 8gb");
        steps.navigateToCart();
        steps.validateCartPageVisible();
        steps.deleteProduct("Dell i7 8gb");
        purchaseModel.setProductPrice(steps.storeProductPriceFromCart("Sony vaio i5"));
        purchaseModel.setTotalAmountCart(steps.storeTotalAmountFromCart());
        steps.validateTotalAmountFromCart(purchaseModel.getProductPrice(), purchaseModel.getTotalAmountCart());
        steps.navigateToPlaceOrder();
        steps.validateOrderFormVisible();
        steps.fillOrderFormAndPurchase();
        steps.validateConfirmationPageVisible();
        confirmation = steps.storeConfirmationInfo();
        steps.validatePurchaseAmount(purchaseModel.getTotalAmountCart(), confirmation.getTotalAmountConfirmation());
        steps.finishPurchaseProcess();
    }

}
