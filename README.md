# Adidas

This project executes one set of test (end to end) against one Web application [demoblaze](https://www.demoblaze.com/).
This project was developed using **Serenity BDD, Maven, Java and JUnit**.

## Getting started

Here are the topics in which the project consist:

1. [Test Plan](#test-plan)
2. [Execute test in your local computer](#execute-test-in-your-local-computer)

## Test Plan

**Product:** Adidas WEB  
**Resposable:** Jose Antonio Lopez Torres  
**Role:** QA Automation  
**System under test:** Web application [demoblaze](https://www.demoblaze.com/)  
**Environment:** Locally   
**Browser** Chrome 97  
**Features to be tested:**  

- Navigate through all categories
- Add and remove product to the cart
- Total purchase amount calculation
- Place an order
- Purchase confirmation values

**Test Coverage:**

- End to End Tests

### End to End Tests

| No. | Scenario |
| ------ | ------ |
| 1 | Validate general navigation, add and remove articles and place an order |

## Execute test in your local computer

1. First you will need to have installed on your computer the next tools:

- **Git 2.32 or latest**
- **Java JDK 8 or latest**
- **Maven 3.5 or latest**
- **Chrome browser installed 97**

2.1. Clone the repository

**FOR THIS OPTION YOU MIGHT NEED PERMISSIONS, PLEASE ASK FOR THEM PROVIDING A GITLAB ACCOUNT**

Open GIT Bash terminal from your computer and **locate your self on the location in which you want to download the
project** and write:

- SSH `git clone git@gitlab.com:jantoniolopezt/adidas_web.git`
- HTTPS `git clone https://gitlab.com/jantoniolopezt/adidas_web.git`

2.2 Download the project as a zip file using the "Download" button from above.

3. Execute tests

- After you cloned or unzip the project a **new folder with the name 'adias_web' will be created**, go inside that
  folder.
- **On a terminal**, locate your self **inside 'adidas_web' folder** and write the next command **`mvn clean compile`**
  and click enter, wait until it finished with a **'Build success'**.
- After that write **`mvn verify`** and click enter, wait until it finished with a **'Build success'**.
- From the logs you will see at the end the URL from which you can open the report with the test results similar to **'
  {home}/adidas_web/target/site/serenity/index.html'**, copy and paste that URL in your favorite browser to see the **
  Test results**.
- Or A **new folder with the name 'target' will be created**, go inside **'target > site > serenity'** and you will see
  a **HTML file with the name 'index.html'**
- Open **'index.html' on a browser** and you will see the **Test results**.

![Report](doc/images/report.png)
